const fastify = require("fastify")({
  logger: true
});

fastify.get("/hello", async (request, reply) => {
  reply.type("application/json").code(200);
  return { messageepsi: "hello" };
});

fastify.listen(8080, "0.0.0.0",  (err, address) => {
  if (err) throw err;
  fastify.log.info(`server listening on ${address}`);
});
